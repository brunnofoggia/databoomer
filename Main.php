<?php

namespace DataBoomer;

/**
 * Utility to keep the data of a object through requests
 *
 * @category	Utilities
 * @author	Bruno Foggia
 * @link	https://bitbucket.org/brunnofoggia/databoomer
 */
trait Main {

    /** Unique ID to identify object data */
//    protected $uid = null;

    /** All data stored here will be kept */
//    protected $data = [];

    /**
     * Restore saved data
     * @param string $uid
     */
    public function unserialize($uid = null) {
        if (empty($uid)) {
            $uid = $this->generateUID();
        }

        $this->data = ($data = static::restoreData($uid)) !== null ? $data : static::defaultData();
        $this->uid = $uid;
    }

    /**
     * Store data
     */
    public function serialize() {
        if (!empty($this->uid)) {
            static::storeData($this->uid, serialize($this->data));
        }
    }

    /**
     * Get UID
     */
    public function getUID() {
        return $this->uid;
    }

    /**
     * Get Data or a item into it
     * @param string key used to get a specific item into data
     */
    public function getData($key = null) {
        empty($this->data) && ($this->data=[]);
        
        if (!$key) {
            return $this->data;
        }

        $keys = explode('.', $key);
        $x = 0;
        $value = $this->data;
        do {
            $value = is_array($value) && array_key_exists($keys[$x], $value) ? $value[$keys[$x]] : null;
        } while (++$x < count($keys));

        return $value;
    }

    /**
     * Add a item and value into data
     * @param string $key
     * @param mixed $value
     */
    public function _addData($key, $value, $stack = null) {
        $keys = is_string($key) ? explode('.', $key) : $key;
        $x = 0;
        $data = $stack === null ? $this->data : $stack;

        if (!count($keys)) {
            return $value;
        }

        $k = array_shift($keys);
        if (!array_key_exists($k, $data) && count($keys) > 0) {
            $data[$k] = [];
        }

        $data[$k] = $this->_addData($keys, $value, array_key_exists($k, $data) && $data[$k]!==null ? $data[$k] : false);
        
        if ($stack === null) {
            $this->data = $data;
        }

        return $data;
    }
    
    public function addData($key, $value, $stack = null) {
        return $this->_addData($key, $value, $stack);
    }

    /**
     * Clear Data
     */
    public function eraseData() {
        !empty($this->uid) && static::clearStoredData($this->uid);
        $this->uid = null;
    }

    /**
     * Apply array function to some data
     */
    public function arrayApply($key, $func, Array $arguments = []) {
        !is_array($this->getData($key)) && $this->addData($key, []);
        $data = $this->getData($key);
        $arguments = array_merge([&$data], $arguments);
        
        $result = call_user_func_array($func, $arguments);
        $this->addData($key, $data);

        return $result;
    }

    /**
     * Will define default data for a new object
     */
    public static function defaultData() {
        return [];
    }

}
